	.global _start
	
_start:

@Display your first and last name and the four digits of your student ID

	MOV R7, #4
	MOV R0, #1
	MOV R2, #21
	LDR R1, =string1
	SWI 0

@Prompt the user for the last four digits of their student ID

	MOV R7, #4
	MOV R0, #1
	MOV R2, #54
	LDR R1, =string2
	SWI 0
	
@Get input form the keyboard

_read:
	MOV R7, #3
	MOV R0, #0
	MOV R2, #5
	LDR R1, =string3
	SWI 0
	
@Validate that four characters were entered
	
	LDR R2, =string3
	LDRB R3, [R2, #4]
	CMP R3, #10
	BEQ _same

@If four characters were not entered then display an error message

	MOV R7, #4
	MOV R0, #1
	MOV R2, #31
	LDR R1, =string4
	SWI 0
	BAL _exit
	
@Else you have four characters then

_same:
	
@Take each of the ASCII digits, convert to decimal and add together
	
	LDR R2, =string3
	LDRB R0, [R2, #0]
	SUB R0, R0, #48
	LDRB R1, [R2, #1]
	SUB R1, R1, #48
	ADD R0, R0, R1
	LDRB R1, [R2, #2]
	SUB R1, R1, #48
	ADD R0, R0, R1
	LDRB R1, [R2, #3]
	SUB R1, R1, #48
	ADD R3, R0, R1

	CMP R3, #18
	BEQ _equal
	BLT _less
	BGT _greater
	
@If the value is less than 18, display the letter L

_less:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #2
	LDR R1, =output1
	SWI 0
	BAL _exit

@If the value equals 18, display the letter E

_equal:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #2
	LDR R1, =output2
	SWI 0
	BAL _exit
	
@If the value is greater than 18, diplay the letter G

_greater:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #2
	LDR R1, =output3
	SWI 0

_exit:

	MOV R7, #1
	SWI 0
	
.data
string1:
.ascii "James Sutherlin 7004\n"

.data
string2:
.ascii "Please enter the last four digits of your student ID:\n"

.data
string3:
.ascii "     "

.data
string4:
.ascii "You did not enter four digits.\n"

.data
output1:
.ascii "L\n"

.data
output2:
.ascii "E\n"

.data
output3:
.ascii "G\n"

